/*
 * QEMU gdium emulation
 *
 * Copyright (c) 2009 yajin <yajin@vm-kernel.org>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

 /*
  * Gdium is a loongson 2f CPU based notebook. http://www.gdium.com/
  */

#include "hw.h"
#include "pc.h"
#include "fdc.h"
#include "net.h"
#include "boards.h"
#include "smbus.h"
#include "block.h"
#include "flash.h"
#include "mips.h"
#include "qemu-char.h"
#include "sysemu.h"
#include "audio/audio.h"
#include "boards.h"
#include "mips_loongson2f.h"
#include "sm502.h"

//#define DEBUG                   /*global debug on/off */
#define DEBUG_BOARD_INIT				 (1<<0x0)
#define DEBUG_I2C_EMU					 (1<<0x1)
#define DEBUG_RAM_MAPPING			 (1<<0x2)

#define DEBUG_ALL                         (0xffffffff)
#define  DEBUG_FLAG                    (DEBUG_BOARD_INIT|DEBUG_RAM_MAPPING)

#ifdef DEBUG
#define debug_out(flag,out) {\
	if (flag & DEBUG_FLAG) printf out; \
	} while(0);
#else
#define debug_out(flag,out)
#endif

#ifdef BIOS_SIZE
#undef BIOS_SIZE
#endif
#define BIOS_SIZE (512 * 1024)
#define BIOS_SECTOR_BITS  (12)
#define GDIUM_SDRAM_SIZE   (512*1024*1024)

#define SM502_DEV_NO         14
#define GDIUM_PCI_MEM_BASE    (0x10000000)

#define GDIUM_GPIO_SCL           6
#define GDIUM_GPIO_SDA         13

#define IDLE                  1
#define START              2
#define RECV_DATA    3
#define SEND_DATA    4
#define SEND_ACK      5

struct emu_i2c
{
    i2c_bus *emu_i2c_bus;
    qemu_irq *emu_i2c_cbs;
    qemu_irq sda_irq;

    uint8_t out_data;
    int8_t out_offset;
    uint8_t in_data;
    int8_t in_offset;

    uint8_t scl;
    uint8_t i2c_state;
    uint8_t waiting_ack;
    uint8_t is_slave_addr;

    uint8_t slave_addr;

};

struct gdium_s
{
    CPUState *env;
    PCIBus *pci_bus;
    struct sm502_s *sm502;
    uint8_t *eeprom_buf;

    struct emu_i2c *emu_i2c;
};

static void main_cpu_reset(void *opaque)
{
    CPUState *env = opaque;
    cpu_reset(env);

    /* gdium stuff */
}

/*SCL=1 SDA:1->0*/
static int is_start(struct emu_i2c *emu_i2c, int line, int level)
{
    if ((line == GDIUM_GPIO_SDA) && (!level) && (emu_i2c->scl))
        return 1;
    return 0;
}

/*SCL=1 SDA:0->1*/
static int is_stop(struct emu_i2c *emu_i2c, int line, int level)
{
    if ((line == GDIUM_GPIO_SDA) && (level) && (emu_i2c->scl))
        return 1;
    return 0;
}

/*SCL: 0->1*/
static int one_cycle(struct emu_i2c *emu_i2c, int line, int level)
{
    if ((line == GDIUM_GPIO_SCL) && (level))
        return 1;
    return 0;
}

/*PMON puts the data on GPIO SDA line*/
static void send_bit(struct gdium_s *s, int line, int level)
{
    s->emu_i2c->out_data |=
        sm502_gpio_get_pin(s->sm502,
                           GDIUM_GPIO_SDA) << (s->emu_i2c->out_offset);
    s->emu_i2c->out_offset--;
}

/*prepare ack to cheat pmon*/
static void prepare_ack(struct gdium_s *s)
{
    /*Set SDA=0 means we received ACK from slave */
    sm502_gpio_set_pin(s->sm502, GDIUM_GPIO_SDA, 0);
}

static void recv_bit(struct gdium_s *s, int line, int level)
{
    if (s->emu_i2c->in_offset == 7)
    {
        s->emu_i2c->in_data = i2c_recv(s->emu_i2c->emu_i2c_bus);
    }

    /*SCL:0->1 */
    qemu_set_irq(s->emu_i2c->sda_irq,
                 (s->emu_i2c->in_data >> (s->emu_i2c->in_offset)) & 0x1);
    s->emu_i2c->in_offset--;
}

static void i2c_emu_cb(void *opaque, int line, int level)
{
    struct gdium_s *s = (struct gdium_s *) opaque;

    if (line == GDIUM_GPIO_SCL)
        s->emu_i2c->scl = level;

    debug_out(DEBUG_I2C_EMU,
              ("line %s LEVEL %d \n", line == GDIUM_GPIO_SCL ? "SCL" : "SDA",
               level));

    /*if we are waiting ack from slave address */
    if (s->emu_i2c->waiting_ack)
    {
        debug_out(DEBUG_I2C_EMU, ("waiting_ack \n"));
        prepare_ack(s);
        if (one_cycle(s->emu_i2c, line, level))
        {
            s->emu_i2c->waiting_ack = 0;
        }
        return;
    }

    /*it means after sending ack, pmon writes to SDA-> wants STOP */
    if ((line == GDIUM_GPIO_SDA) && (s->emu_i2c->i2c_state == RECV_DATA))
    {
        s->emu_i2c->i2c_state = IDLE;
        return;
    }

    if ((s->emu_i2c->i2c_state == IDLE) || (s->emu_i2c->i2c_state == SEND_DATA))
    {
        /*check whether start signal */
        if (is_start(s->emu_i2c, line, level))
        {
            s->emu_i2c->i2c_state = SEND_DATA;
            /*is_slave_addr=1 means the next byte is slave addreses */
            s->emu_i2c->is_slave_addr = 1;
            s->emu_i2c->i2c_state = SEND_DATA;
            debug_out(DEBUG_I2C_EMU,
                      ("I2C Start. state %d \n", s->emu_i2c->i2c_state));
            s->emu_i2c->out_offset = 7;
            s->emu_i2c->out_data = 0;
            s->emu_i2c->slave_addr = 0;

            return;
        }
    }
    if (is_stop(s->emu_i2c, line, level))
    {
        debug_out(DEBUG_I2C_EMU, ("STOP \n"));
        s->emu_i2c->i2c_state = IDLE;
        i2c_end_transfer(s->emu_i2c->emu_i2c_bus);
        return;
    }

    if (one_cycle(s->emu_i2c, line, level))
    {
        debug_out(DEBUG_I2C_EMU, ("one cycle \n"));
        switch (s->emu_i2c->i2c_state)
        {
        case IDLE:
            debug_out(DEBUG_I2C_EMU, ("IDLE \n"));
            break;
        case SEND_DATA:
            send_bit(s, line, level);
            if (s->emu_i2c->out_offset < 0)
            {
                debug_out(DEBUG_I2C_EMU,
                          ("slave_addr %d out data %d \n",
                           s->emu_i2c->slave_addr, s->emu_i2c->out_data));
                if (s->emu_i2c->is_slave_addr)
                {
                    /*first send is slave address */
                    s->emu_i2c->slave_addr = s->emu_i2c->out_data;
                    i2c_start_transfer(s->emu_i2c->
                                       emu_i2c_bus,
                                       s->emu_i2c->slave_addr & 0xfe,
                                       s->emu_i2c->slave_addr & 0x1);
                    debug_out(DEBUG_I2C_EMU,
                              ("slave_addr  %d \n", s->emu_i2c->slave_addr));

                    s->emu_i2c->is_slave_addr = 0;
                }
                else
                {
                    /*send reg address or data */
                    i2c_send(s->emu_i2c->emu_i2c_bus, s->emu_i2c->out_data);
                }
                s->emu_i2c->out_offset = 7;
                s->emu_i2c->out_data = 0;
                if (s->emu_i2c->slave_addr & 0x1)
                {
                    s->emu_i2c->in_offset = 7;
                    s->emu_i2c->i2c_state = RECV_DATA;
                }
                else
                    s->emu_i2c->i2c_state = SEND_DATA;
                s->emu_i2c->waiting_ack = 1;

            }
            break;
        case RECV_DATA:
            /*RECV. TODO */
            recv_bit(s, line, level);
            if (s->emu_i2c->in_offset < 0)
                s->emu_i2c->i2c_state = SEND_ACK;
            break;
        case SEND_ACK:
            debug_out(DEBUG_I2C_EMU, ("SEND_ACK \n"));
            s->emu_i2c->in_offset = 7;
            s->emu_i2c->i2c_state = RECV_DATA;
            i2c_nack(s->emu_i2c->emu_i2c_bus);
            break;
        default:
            fprintf(stderr, "unknown state %d \n", s->emu_i2c->i2c_state);
            exit(-1);
        }

    }

}

/*gdium uses SM502 GPIO 6/13 to emulate I2C.*/
static void mips_gdium_i2c_emu(struct gdium_s *s)
{
    s->emu_i2c->emu_i2c_cbs = qemu_allocate_irqs(i2c_emu_cb, s, 32);
    sm502_gpio_out_set(s->sm502, GDIUM_GPIO_SCL,
                       s->emu_i2c->emu_i2c_cbs[GDIUM_GPIO_SCL]);
    sm502_gpio_out_set(s->sm502, GDIUM_GPIO_SDA,
                       s->emu_i2c->emu_i2c_cbs[GDIUM_GPIO_SDA]);

    s->emu_i2c->i2c_state = IDLE;

    s->emu_i2c->sda_irq = sm502_gpio_in_get(s->sm502, GDIUM_GPIO_SDA)[0];
}

/*dumped from my gdium pcb*/
uint8_t gdium_spd[0x80] = {
    0x80, 0x08, 0x08, 0x0e, 0x0a, 0x60, 0x40, 0x00,     /*0x00-0x07 */
    0x05, 0x3d, 0x50, 0x00, 0x82, 0x00, 0x00, 0x00,     /*0x08-0x0f */
    0x0c, 0x04, 0x38, 0x00, 0x04, 0x00, 0x01, 0x3d,     /*0x10-0x17 */
    0x50, 0x50, 0x60, 0x3c, 0x1e, 0x3c, 0x2d, 0x80,     /*0x18-0x1f */
    0x25, 0x37, 0x10, 0x22, 0x3c, 0x1e, 0x13, 0x00,     /*0x20-0x27 */
    0x00, 0x3c, 0x69, 0x80, 0x1e, 0x28, 0x00, 0x00,     /*0x28-0x2f */
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,     /*0x30-0x37 */
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x10, 0xb2,     /*0x38-0x3f */
    0x7f, 0x4f, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,     /*0x40-0x47 */
    0x54, 0x4a, 0x4d, 0x35, 0x33, 0x33, 0x51, 0x53,     /*0x48-0x4f */
    0x4a, 0x2d, 0x35, 0x31, 0x32, 0x4d, 0x20, 0x20,     /*0x50-0x57 */
    0x20, 0x20, 0x20, 0x00, 0x00, 0x07, 0x47, 0x00,     /*0x58-0x5f */
    0x07, 0x00, 0xb1, 0x98, 0x00, 0x00, 0x00, 0x00,     /*0x60-0x67 */
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,     /*0x68-0x6f */
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,     /*0x70-0x77 */
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,     /*0x78-0x7f */
};

static
    void mips_gdium_init(ram_addr_t ram_size, int vga_ram_size,
                         const char *boot_device,
                         const char *kernel_filename,
                         const char *kernel_cmdline,
                         const char *initrd_filename, const char *cpu_model)
{
    int index;
    unsigned long bios_offset;
    uint8_t *eeprom_buf;

    struct gdium_s *s = qemu_mallocz(sizeof(*s));

    if (cpu_model == NULL)
    {
        cpu_model = "Loongson-2F";
    }
    s->env = cpu_init(cpu_model);
    if (!s->env)
    {
        fprintf(stderr, "Unable to find CPU definition\n");
        exit(1);
    }

    /*set bit 5:7 of status registerto 1.
     * Loongson 2f does not have 32bit address space and ux/sx/kx is not used in loongson
     * But qemu needs this. So just set these bits to 1 
     */
    s->env->CP0_Status |= 0xe0;

    register_savevm("cpu", 0, 3, cpu_save, cpu_load, s->env);
    qemu_register_reset(main_cpu_reset, s->env);

    /* allocate RAM */
    if (ram_size > (GDIUM_SDRAM_SIZE))
    {
        fprintf(stderr,
                "qemu: Too much memory for this machine: %d MB, maximum 512 MB\n",
                ((unsigned int) ram_size / (1 << 20)));
        exit(1);
    }

    /*CPU ADDRESS:[0:256M]<->DDR [0:256M]
     * DDR_ADDR[256M:512M] mapping determined by loongson 2f address windows configuration.
     */
    if (ram_size > (256 * 1024 * 1024))
    {
        debug_out(DEBUG_RAM_MAPPING, ("Mapping first 256M RAM\n"));
        cpu_register_physical_memory(0, (256 * 1024 * 1024), IO_MEM_RAM);
    }
    else
        cpu_register_physical_memory(0, ram_size, IO_MEM_RAM);

    bios_offset = ram_size + vga_ram_size;
    cpu_register_physical_memory(0x1fc00000LL, BIOS_SIZE,
                                 bios_offset | IO_MEM_ROM);

    /*sst39vf040 flash */
    index = drive_get_index(IF_PFLASH, 0, 0);
    if (index == -1)
    {
        fprintf(stderr,
                "A flash image must be given with the " "'pflash' parameter\n");
        exit(1);
    }

    debug_out(DEBUG_BOARD_INIT, ("Register sst39vf040  size %x  at "
                                 "offset %08lx addr %08llx '%s' %x\n",
                                 BIOS_SIZE, bios_offset, 0x1fc00000LL,
                                 bdrv_get_device_name(drives_table[index].bdrv),
                                 (BIOS_SIZE >> BIOS_SECTOR_BITS)));
    pflash_cfi02_register(0x1fc00000LL, bios_offset, drives_table[index].bdrv,
                          (1 << BIOS_SECTOR_BITS),
                          BIOS_SIZE >> BIOS_SECTOR_BITS, 1, 1, 0xbf, 0xd7,
                          0x0000, 0x0000, 0x555, 0xaaa);

    cpu_mips_irq_init_cpu(s->env);
    cpu_mips_clock_init(s->env);

    s->pci_bus = bonito_init(s->env);
    s->sm502 = sm502_init(s->pci_bus, SM502_DEV_NO << 3, 0x10000000);
    loongson_ddr2_init();
    loongson_addwin_init();

    s->emu_i2c = qemu_mallocz(sizeof(*s->emu_i2c));
    s->emu_i2c->emu_i2c_bus = i2c_init_bus();
    /*DDR SPD EEPROM */
    eeprom_buf = qemu_mallocz(8 * 256); /* XXX: make this persistent */
    memcpy(eeprom_buf, gdium_spd, sizeof(gdium_spd));
    smbus_eeprom_device_init(s->emu_i2c->emu_i2c_bus, 0xa2, eeprom_buf);

    /*rtc */
    m41t80_init(s->emu_i2c->emu_i2c_bus, NULL, 0xd0);
    /*temperature sensors */
    stds75_init(s->emu_i2c->emu_i2c_bus, NULL, 0x90);

    mips_gdium_i2c_emu(s);

}

QEMUMachine mips_gdium_machine = {
    .name = "gdium",
    .desc = "MIPS Gdium notebook",
    .init = mips_gdium_init,
    .ram_require = VGA_RAM_SIZE + BIOS_SIZE,
    .nodisk_ok = 1,
};
