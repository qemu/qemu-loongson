/*
 * QEMU loongson 2f emulation
 *
 * Copyright (c) 2009 yajin <yajin@vm-kernel.org>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */


#ifndef __HW_LOONGSON2F_H__
#define __HW_LOONGSON2F_H__

#include "hw.h"
#include "mips.h"
#include "pc.h"

#include "pci.h"


#define DEBUG_INVALID_WIDTH			1

#ifdef DEBUG_INVALID_WIDTH

#define LOONGSON_8B_REG(paddr)		\
        fprintf(stderr, "%s: 8-bit register " TARGET_FMT_plx "\n",	\
                        __FUNCTION__, paddr)
#define LOONGSON_16B_REG(paddr)		\
        fprintf(stderr, "%s: 16-bit register " TARGET_FMT_plx "\n",	\
                        __FUNCTION__, paddr)
#define LOONGSON_32B_REG(paddr)		\
        fprintf(stderr, "%s: 32-bit register " TARGET_FMT_plx "\n",	\
                        __FUNCTION__, paddr)
#define LOONGSON_RO_REG(paddr)		\
        fprintf(stderr, "%s: write to read only 32-bit register " TARGET_FMT_plx "\n",	\
                        __FUNCTION__, paddr)
#define LOONGSON_WO_REG(paddr)		\
        fprintf(stderr, "%s: read from write only 32-bit register " TARGET_FMT_plx "\n",	\
                        __FUNCTION__, paddr)
#else
#define LOONGSON_8B_REG(paddr)
#define LOONGSON_16B_REG(paddr)
#define LOONGSON_32B_REG(paddr)
#define LOONGSON_RO_REG(paddr)
#define LOONGSON_WO_REG(paddr)
#endif


PCIBus *bonito_init(CPUState * env);
void loongson_ddr2_init(void);
void loongson_addwin_init(void);

#endif
