/*
 * QEMU sm502 emulation
 *
 * Copyright (c) 2009 yajin <yajin@vm-kernel.org>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */


#ifndef __HW_SM502_H__
#define __HW_SM502_H__

#include "hw.h"
#include "pc.h"
#include "pci.h"

#define DEBUG_INVALID_WIDTH 1
#ifdef DEBUG_INVALID_WIDTH

#define SM502_8B_REG(paddr)		\
        fprintf(stderr, "%s: 8-bit register " TARGET_FMT_plx "\n",	\
                        __FUNCTION__, paddr)
#define SM502_16B_REG(paddr)		\
        fprintf(stderr, "%s: 16-bit register " TARGET_FMT_plx "\n",	\
                        __FUNCTION__, paddr)
#define SM502_32B_REG(paddr)		\
        fprintf(stderr, "%s: 32-bit register " TARGET_FMT_plx "\n",	\
                        __FUNCTION__, paddr)
#define SM502_RO_REG(paddr)		\
        fprintf(stderr, "%s: write to read only 32-bit register " TARGET_FMT_plx "\n",	\
                        __FUNCTION__, paddr)
#define SM502_WO_REG(paddr)		\
        fprintf(stderr, "%s: read from write only 32-bit register " TARGET_FMT_plx "\n",	\
                        __FUNCTION__, paddr)
#else
#define SM502_8B_REG(paddr)
#define SM502_16B_REG(paddr)
#define SM502_32B_REG(paddr)
#define SM502_RO_REG(paddr)
#define SM502_WO_REG(paddr)
#endif

struct sm502_s
{
    PCIDevice dev;
    DisplayState *ds;

    /*SM502 System Configuration Register */
    uint32_t system_control;    /*0x00 */
    uint32_t misc_control;      /*0x04 */
    uint32_t gpio_low32;        /*0x08 */
    uint32_t gpio_high32;       /*0x0c */
    uint32_t dram_control;      /*0x10 */
    uint32_t arb_control;       /*0x14 */
    uint32_t command_list_status;       /*0x24 */
    uint32_t raw_int_status;    /*0x28 */
    uint32_t int_status;        /*0x2c */
    uint32_t int_mask;          /*0x30 */
    uint32_t debug_control;     /*0x34 */
    uint32_t current_gate;      /*0x38 */
    uint32_t current_clock;     /*0x3c */
    uint32_t power0_gate;       /*0x40 */
    uint32_t power0_clock;      /*0x44 */
    uint32_t power1_gate;       /*0x48 */
    uint32_t power1_clock;      /*0x4c */
    uint32_t sleep_gate;        /*0x50 */
    uint32_t power_control;     /*0x54 */
    uint32_t pci_master_base;   /*0x58 */
    uint32_t endian_control;    /*0x5c */
    uint32_t device_id;         /*0x60 */
    uint32_t pll_clock_count;   /*0x64 */
    uint32_t misc_timing;       /*0x68 */
    uint32_t current_sdram_clock;       /*0x6c */
    uint32_t non_cache_address; /*0x70 */
    uint32_t pll_control;       /*0x74 */


     /*GPIO*/ qemu_irq out_handler[64];
    qemu_irq *in_handler;
    uint32_t gpio_data_low;     /*0x010000 */
    uint32_t gpio_data_high;    /*0x010004 */
    uint32_t gpio_datadir_low;  /*0x010008 */
    uint32_t gpio_datadir_high; /*0x01000c */
    uint32_t gpio_int_setup;    /*0x010010 */
    uint32_t gpio_int_status;   /*0x01001c */

    uint32_t sm502_mm_io;       /*MMIO base address */
    int mmio_index;
};


struct sm502_s *sm502_init(PCIBus * bus, int devfn,
                           target_phys_addr_t pci_mem_base);
void sm502_gpio_out_set(struct sm502_s *s, int line, qemu_irq handler);
qemu_irq *sm502_gpio_in_get(struct sm502_s *s, int line);
void sm502_gpio_set_pin(struct sm502_s *s, int line, int level);
int sm502_gpio_get_pin(struct sm502_s *s, int line);


#endif
