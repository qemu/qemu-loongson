/*
 * QEMU loongson 2f emulation
 *
 * Copyright (c) 2009 yajin <yajin@vm-kernel.org>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#include "hw.h"
#include "sysemu.h"
#include "mips_loongson2f.h"

typedef target_phys_addr_t pci_addr_t;
#include "pci_host.h"

 /*The bonito north bridge is integrated in loongson 2f */
typedef struct bonitoState
{
    PCIHostState *pci;
    CPUState *env;

    /*north brige pci config */
    uint32_t config[64];


    uint32_t poncfg;            /*0x100 */
    uint32_t gencfg;            /*0x104 */
    uint32_t liocfg;            /*0x108 */
    uint32_t pcimap;            /*0x110 */
    uint32_t pcix_bridge_cfg;   /*0x114 */
    uint32_t pcimap_cfg;        /*0x118 */
    uint32_t gpio_data;         /*0x11c */
    uint32_t gpio_en;           /*0x120 */
    uint32_t intedge;           /*0x124 */
    uint32_t intpol;            /*0x12c */
    uint32_t intenset;          /*0x130 */
    uint32_t intenclr;          /*0x134 */
    uint32_t inten;             /*0x138 */
    uint32_t intisr;            /*0x13c */
    uint32_t mem_win_base_l;    /*0x140 */
    uint32_t mem_win_base_h;    /*0x144 */
    uint32_t mem_win_mask_l;    /*0x148 */
    uint32_t mem_win_mask_h;    /*0x14c */
    uint32_t pci_hit0_sel_l;    /*0x150 */
    uint32_t pci_hit0_sel_h;    /*0x154 */
    uint32_t pci_hit1_sel_l;    /*0x158 */
    uint32_t pci_hit1_sel_h;    /*0x15c */
    uint32_t pci_hit2_sel_l;    /*0x160 */
    uint32_t pci_hit2_sel_h;    /*0x164 */
    uint32_t pxarb_config;      /*0x168 */
    uint32_t pxarb_status;      /*0x16c */
    uint32_t chip_config0;      /*0x180 */
    uint32_t pad1v8_ctrl;       /*0x184 */
    uint32_t pad3v3_ctrl;       /*0x188 */
    uint32_t comp_code;         /*0x190 */
    uint32_t chip_sample1;      /*0x194 */


    /*util */
    uint32_t pci_bus_no;
    uint32_t pci_dev_no;
} bonito_state;


//#define DEBUG                   						/*global debug on/off */
#define DEBUG_BONITO                     (1<<0x0)
#define DEBUG_DDR2                        (1<<0x1)
#define DEBUG_ADDR_WIN                (1<<0x2)

#define DEBUG_ALL                         (0xffffffff)
#define  DEBUG_FLAG                    DEBUG_ADDR_WIN

#ifdef DEBUG
#define debug_out(flag,out) {\
	if (flag & DEBUG_FLAG) printf out; \
	} while(0);
#else
#define debug_out(flag,out)
#endif


#if 0
uint32_t loongson_badwidth_read8(void *opaque, target_phys_addr_t addr)
{
    LOONGSON_8B_REG(addr);
    /*to make debug easy */
    exit(-1);
    return 0;
}
void loongson_badwidth_write8(void *opaque, target_phys_addr_t addr,
                              uint32_t value)
{
    LOONGSON_8B_REG(addr);
    exit(-1);
}

uint32_t loongson_badwidth_read16(void *opaque, target_phys_addr_t addr)
{
    LOONGSON_16B_REG(addr);
    /*to make debug easy */
    exit(-1);
    return 0;
}
void loongson_badwidth_write16(void *opaque, target_phys_addr_t addr,
                               uint32_t value)
{
    LOONGSON_16B_REG(addr);
    exit(-1);
}
#endif
static uint32_t loongson_badwidth_read32(void *opaque, target_phys_addr_t addr)
{
    LOONGSON_32B_REG(addr);
    /*to make debug easy */
    exit(-1);
    return 0;
}
static void loongson_badwidth_write32(void *opaque, target_phys_addr_t addr,
                               uint32_t value)
{
    LOONGSON_32B_REG(addr);
    exit(-1);
}




static void bonito_reset(void *opaque)
{
    bonito_state *s = opaque;

    /* set the default value of north bridge pci config */
    s->config[0x00] = 0xd5df53;
    s->config[0x02] = 0x06000001;
    s->config[0xf] = 0x3c0100;

    s->liocfg = 0x1ffc;
    s->pcix_bridge_cfg = 0x18;
    s->gpio_en = 0xf;
    s->pci_hit0_sel_l = 0x6;
    s->pci_hit0_sel_h = 0x80000000;
    s->pci_hit1_sel_l = 0x6;
    s->pci_hit1_sel_h = 0x80000000;
    s->pci_hit2_sel_l = 0x6;
    s->pci_hit2_sel_h = 0x80000000;
    s->pxarb_config = 0x1c5;
    s->chip_config0 = 0x617;
    s->pad1v8_ctrl = 0xf80;
    s->pad3v3_ctrl = 0xf80;
}

/*when the device on the pci bus call dev->irq, pci bus will call pci_bonito_set_irq*/
static void bonito_set_irq(qemu_irq * pic, int irq_num, int level)
{
    printf("%s : unimplemented \n", __FUNCTION__);
    exit(0);
}

/*map the irq no*/
static int bonito_map_irq(PCIDevice * pci_dev, int irq_num)
{
    printf("%s : unimplemented \n", __FUNCTION__);
    exit(0);
}

#if 0
static uint32_t bonito_pci_read32(void *opaque, target_phys_addr_t addr)
{
    bonito_state *s = (bonito_state *) opaque;
    return (s->config[addr >> 2]);
}

static void bonito_pci_write32(void *opaque, target_phys_addr_t addr,
                               uint32_t value)
{

    bonito_state *s = (bonito_state *) opaque;
    uint32_t offset;

    switch (addr)
    {
    case 0x10:
    case 0x14:
    case 0x18:
    case 0x1c:
    case 0x20:
    case 0x24:
    case 0x40:
    case 0x44:
    case 0x48:
    case 0x4c:
    case 0x50:
    case 0x54:
    case 0x58:
    case 0x5c:
    case 0x60:
    case 0x64:
    case 0x68:
    case 0x6c:
        s->config[addr >> 2] = value;
        break;
    default:
        cpu_abort(s->env,
                  "bonito_pci_write32 undefined addr " TARGET_FMT_plx
                  "  value %x \n", addr, value);

    }
}


CPUReadMemoryFunc *bonito_pci_readfn[] = {
    loongson_badwidth_read32,
    loongson_badwidth_read32,
    bonito_pci_read32,
};
CPUWriteMemoryFunc *bonito_pci_writefn[] = {
    loongson_badwidth_write32,
    loongson_badwidth_write32,
    bonito_pci_write32,
};
#endif
static uint32_t bonito_read32(void *opaque, target_phys_addr_t addr)
{
    bonito_state *s = (bonito_state *) opaque;

    /*bonito pci config */
    if (addr < 0x100)
        return (s->config[addr >> 2]);

    switch (addr)
    {
    case 0x100:
        return s->poncfg;
    case 0x104:
        return s->gencfg;
    case 0x108:
        return s->liocfg;
    case 0x110:
        return s->pcimap;
    case 0x114:
        return s->pcix_bridge_cfg;
    case 0x118:
        return s->pcimap_cfg;
    case 0x11c:
        return s->gpio_data;
    case 0x120:
        return s->gpio_en;
    case 0x124:
        return s->intedge;
    case 0x12c:
        return s->intpol;
    case 0x130:
        return s->intenset;
    case 0x134:
        return s->intenclr;
    case 0x138:
        return s->inten;
    case 0x13c:
        return s->intisr;
    case 0x140:
        return s->mem_win_base_l;
    case 0x144:
        return s->mem_win_base_h;
    case 0x148:
        return s->mem_win_mask_l;
    case 0x14c:
        return s->mem_win_mask_h;
    case 0x150:
        return s->pci_hit0_sel_l;
    case 0x154:
        return s->pci_hit0_sel_h;
    case 0x158:
        return s->pci_hit1_sel_l;
    case 0x15c:
        return s->pci_hit1_sel_h;
    case 0x160:
        return s->pci_hit2_sel_l;
    case 0x164:
        return s->pci_hit2_sel_h;
    case 0x168:
        return s->pxarb_config;
    case 0x16c:
        return s->pxarb_status;
    case 0x180:
        return s->chip_config0;
    case 0x184:
        return s->pad1v8_ctrl;
    case 0x188:
        return s->pad3v3_ctrl;
    case 0x190:
        return s->comp_code;
    case 0x194:
        return s->chip_sample1;
    default:
        cpu_abort(s->env,
                  "bonito_read32 undefined addr " TARGET_FMT_plx
                  " pc " TARGET_FMT_plx "\n", addr, s->env->active_tc.PC);

    }
}

static void bonito_write32(void *opaque, target_phys_addr_t addr,
                           uint32_t value)
{
    bonito_state *s = (bonito_state *) opaque;

    debug_out(DEBUG_BONITO,
              ("%s addr " TARGET_FMT_plx " value %x pc " TARGET_FMT_plx "\n",
              __FUNCTION__, addr, value, s->env->active_tc.PC));

    switch (addr)
    {
    case 0x4:
    	 /*we do not write to 0x4. It is the BONITO_PCICMD*/
        break;
    case 0x8:
    case 0xc:
    case 0x10:
    case 0x14:
    case 0x18:
    case 0x1c:
    case 0x20:
    case 0x24:
    case 0x30:
    case 0x3c:
    case 0x40:
    case 0x44:
    case 0x48:
    case 0x4c:
    case 0x50:
    case 0x54:
    case 0x58:
    case 0x5c:
    case 0x60:
    case 0x64:
    case 0x68:
    case 0x6c:
        s->config[addr >> 2] = value;
        break;
    case 0x16c:
        LOONGSON_RO_REG(addr);
        break;
    case 0x100:
        /*0x100 is not read only? */
        break;
    case 0x104:
        s->gencfg = value & 0x1;
        break;
    case 0x108:
        s->liocfg = value & 0x7ffc;
        break;
    case 0x110:
        s->pcimap = value & 0x7ffff;
        break;
    case 0x114:
        s->pcix_bridge_cfg = value & 0x7f;
        break;
    case 0x118:
        s->pcimap_cfg = value & 0x1ffff;
        if (s->pcimap_cfg & 0x8000)
        {
            /*type1 configuration */
            s->pci_bus_no = s->pcimap_cfg & 0xff;
        }
        else
        {
            /*Type 0 configuration */
            s->pci_bus_no = 0;
            /*ffs count from bit 1. So we minus 12 not 11 here */
            s->pci_dev_no = ffs(((s->pcimap_cfg << 16) & 0xfffff800)) - 12;
        }
        break;
    case 0x11c:
        s->gpio_data = value & 0xf000f;
        break;
    case 0x120:
        s->gpio_en = value & 0xf;
        break;
    case 0x150:
        s->pci_hit0_sel_l = value & 0xfffff006;
        break;
    case 0x154:
        s->pci_hit0_sel_h = value;
        break;
    case 0x158:
        s->pci_hit1_sel_l = value & 0xfffff006;
        break;
    case 0x15c:
        s->pci_hit1_sel_h = value;
        break;
    case 0x160:
        s->pci_hit2_sel_l = value & 0xfffff006;
        break;
    case 0x164:
        s->pci_hit2_sel_h = value;
        break;
    case 0x168:
        s->pxarb_config = value & 0xffff;
        break;
    case 0x180:
        s->chip_config0 = value & 0x7ff;
        break;
    case 0x184:
        s->pad1v8_ctrl = value & 0x1ffff;
        break;
    default:
        cpu_abort(s->env,
                  "bonito_write32 undefined addr " TARGET_FMT_plx
                  "  value %x pc " TARGET_FMT_plx "\n", addr, value,
                  s->env->active_tc.PC);
    }

}



CPUReadMemoryFunc *bonito_readfn[] = {
    loongson_badwidth_read32,
    loongson_badwidth_read32,
    bonito_read32,
};

CPUWriteMemoryFunc *bonito_writefn[] = {
    loongson_badwidth_write32,
    loongson_badwidth_write32,
    bonito_write32,
};

static uint32_t bonito_pciconf_read32(void *opaque, target_phys_addr_t addr)
{
    bonito_state *s = (bonito_state *) opaque;
    int fun_no, reg_no;
    uint32_t pci_addr;

    debug_out(DEBUG_BONITO,
              ("%s addr " TARGET_FMT_plx "  pc " TARGET_FMT_plx "\n",
              __FUNCTION__, addr, s->env->active_tc.PC));

    if (s->pcimap_cfg & 0x8000)
    {
        /*type1 configuration */
        s->pci_dev_no = (addr & 0xf800) >> 11;
        fun_no = (addr & 0x700) >> 8;
        reg_no = (addr & 0xff);
    }
    else
    {
        fun_no = (addr & 0x700) >> 8;
        reg_no = (addr & 0xff);
    }

    pci_addr =
        (s->pci_bus_no << 16) | (s->pci_dev_no << 11) | (fun_no << 8) | reg_no;
    debug_out(DEBUG_BONITO,
              ("TYPE%d bus_no 0x%x dev_no 0x%x fun_no 0x%x reg_no 0x%x pci_addr %x \n",
              s->pcimap_cfg & 0x8000 ? 1 : 0, s->pci_bus_no, s->pci_dev_no,
              fun_no, reg_no, pci_addr));

    s->pci->config_reg = (pci_addr) | (1u << 31);
    return pci_host_data_readl(s->pci, 0);

}

static void bonito_pciconf_write32(void *opaque, target_phys_addr_t addr,
                                   uint32_t value)
{
    bonito_state *s = (bonito_state *) opaque;
    int fun_no, reg_no;
    uint32_t pci_addr;

    debug_out(DEBUG_BONITO,
              ("%s addr " TARGET_FMT_plx " value %x pc " TARGET_FMT_plx "\n",
              __FUNCTION__, addr, value, s->env->active_tc.PC));

    if (s->pcimap_cfg & 0x8000)
    {
        /*type1 configuration */
        s->pci_dev_no = (addr & 0xf800) >> 11;
        fun_no = (addr & 0x700) >> 8;
        reg_no = (addr & 0xff);
    }
    else
    {
        fun_no = (addr & 0x700) >> 8;
        reg_no = (addr & 0xff);
    }

    pci_addr =
        (s->pci_bus_no << 16) | (s->pci_dev_no << 11) | (fun_no << 8) | reg_no;
    debug_out(DEBUG_BONITO,
              ("TYPE%d bus_no 0x%x dev_no 0x%x fun_no 0x%x reg_no 0x%x pci_addr %x \n",
              s->pcimap_cfg & 0x8000 ? 1 : 0, s->pci_bus_no, s->pci_dev_no,
              fun_no, reg_no, pci_addr));

    s->pci->config_reg = (pci_addr) | (1u << 31);
    pci_host_data_writel(s->pci, 0, value);


}

CPUReadMemoryFunc *bonito_pciconf_readfn[] = {
    loongson_badwidth_read32,
    loongson_badwidth_read32,
    bonito_pciconf_read32,
};

CPUWriteMemoryFunc *bonito_pciconf_writefn[] = {
    loongson_badwidth_write32,
    loongson_badwidth_write32,
    bonito_pciconf_write32,
};

PCIBus *bonito_init(CPUState * env)
{
    bonito_state *s;
    int iomemtype;

    s = qemu_mallocz(sizeof(*s));
    s->env = env;
    s->pci = qemu_mallocz(sizeof(*s->pci));

    /*get the north bridge pci bus */
    s->pci->bus =
        pci_register_bus(bonito_set_irq, bonito_map_irq, env->irq, 0x28, 64);

  
    iomemtype = cpu_register_io_memory(0, bonito_readfn, bonito_writefn, s);
    cpu_register_physical_memory(0x1fe00000, 4096, iomemtype);

    iomemtype =
        cpu_register_io_memory(0, bonito_pciconf_readfn, bonito_pciconf_writefn,
                               s);
    cpu_register_physical_memory(0x1fe80000, 0x80000, iomemtype);

    /*bonito internal UART. It uses IP3 of MIPS */
    if (serial_hds[0])
        serial_mm_init(0x1ff003f8, 0, env->irq[3], 38400, serial_hds[0], 1);


    bonito_reset(s);

    return s->pci->bus;
}


struct loongson_ddr2_s
{
    uint32_t conf_ctl[64];
};


static uint32_t loongson_ddr2_read32(void *opaque, target_phys_addr_t addr)
{
    struct loongson_ddr2_s *s = (struct loongson_ddr2_s *) opaque;

    debug_out(DEBUG_DDR2,
              ("%s addr " TARGET_FMT_plx "  pc " TARGET_FMT_plx "\n",
              __FUNCTION__, addr, cpu_single_env->active_tc.PC));

    return s->conf_ctl[(addr & 0xff) / 4];
}

static void loongson_ddr2_write32(void *opaque, target_phys_addr_t addr,
                                  uint32_t value)
{
    struct loongson_ddr2_s *s = (struct loongson_ddr2_s *) opaque;

    debug_out(DEBUG_DDR2,
              ("%s addr " TARGET_FMT_plx " value %x pc " TARGET_FMT_plx "\n",
              __FUNCTION__, addr, value, cpu_single_env->active_tc.PC));

    s->conf_ctl[(addr & 0xff) / 4] = value;
}

CPUReadMemoryFunc *loongson_ddr2_readfn[] = {
    loongson_badwidth_read32,
    loongson_badwidth_read32,
    loongson_ddr2_read32,
};

CPUWriteMemoryFunc *loongson_ddr2_writefn[] = {
    loongson_badwidth_write32,
    loongson_badwidth_write32,
    loongson_ddr2_write32,
};



void loongson_ddr2_init()
{
    /*Just a stub because we do not care this currently */
    int iomemtype;
    struct loongson_ddr2_s *s;

    s = qemu_mallocz(sizeof(*s));
    iomemtype =
        cpu_register_io_memory(0, loongson_ddr2_readfn, loongson_ddr2_writefn,
                               s);
    /*In fact it starts from CONFIG_BASE:0xaffffe00,
    but qemu needs this memory PAGE_SIZE aligned*/
    cpu_register_physical_memory(0xffff000,4096, iomemtype);
}

struct loongson_addrwin_s
{
    uint32_t cpu_win_base_low[4];
    uint32_t cpu_win_base_high[4];
    uint32_t cpu_win_mask_low[4];
    uint32_t cpu_win_mask_high[4];
    uint32_t cpu_win_mmap_low[4];
    uint32_t cpu_win_mmap_high[4];

    uint32_t pcidma_win_base_low[4];
    uint32_t pcidma_win_base_high[4];
    uint32_t pcidma_win_mask_low[4];
    uint32_t pcidma_win_mask_high[4];
    uint32_t pcidma_win_mmap_low[4];
    uint32_t pcidma_win_mmap_high[4];
};

static uint32_t loongson_addwin_read32(void *opaque, target_phys_addr_t addr)
{
    struct loongson_addrwin_s *s = (struct loongson_addrwin_s *) opaque;

    debug_out(DEBUG_ADDR_WIN,
              ("%s addr " TARGET_FMT_plx "  pc " TARGET_FMT_plx "\n",
              __FUNCTION__, addr, cpu_single_env->active_tc.PC));
    switch (addr)
    {
    	case 0x00:
    	case 0x08:
    	case 0x10:
    	case 0x18:
    		return s->cpu_win_base_low[addr/0x8];
    	case 0x04:
    	case 0x0c:
    	case 0x14:
    	case 0x1c:
    		return s->cpu_win_base_high[(addr-0x4)/0x8];
    	case 0x20:
    	case 0x28:
    	case 0x30:
    	case 0x38:
    		return s->cpu_win_mask_low[(addr-0x20)/0x8];
    	case 0x24:
    	case 0x2c:
    	case 0x34:
    	case 0x3c:
    		return s->cpu_win_mask_high[(addr-0x24)/0x8];
    	case 0x40:
    	case 0x48:
    	case 0x50:
    	case 0x58:
    		return s->cpu_win_mmap_low[(addr-0x40)/0x8];
    	case 0x44:
    	case 0x4c:
    	case 0x54:
    	case 0x5c:
    		return s->cpu_win_mmap_high[(addr-0x44)/0x8];

    	case 0x60:
    	case 0x68:
    	case 0x70:
    	case 0x78:
    		return s->pcidma_win_base_low[(addr-0x60)/0x8];
    	case 0x64:
    	case 0x6c:
    	case 0x74:
    	case 0x7c:
    		return s->pcidma_win_base_high[(addr-0x64)/0x8];
    	case 0x80:
    	case 0x88:
    	case 0x90:
    	case 0x98:
    		return s->pcidma_win_mask_low[(addr-0x80)/0x8];
    	case 0x84:
    	case 0x8c:
    	case 0x94:
    	case 0x9c:
    		return s->pcidma_win_mask_high[(addr-0x84)/0x8];
    	case 0xa0:
    	case 0xa8:
    	case 0xb0:
    	case 0xb8:
    		return s->pcidma_win_mmap_low[(addr-0xa0)/0x8];
    	case 0xa4:
    	case 0xac:
    	case 0xb4:
    	case 0xbc:
    		return s->pcidma_win_mmap_high[(addr-0xa4)/0x8];
    	default:
        	cpu_abort(cpu_single_env,
                  "%s undefined addr " TARGET_FMT_plx
                  "pc " TARGET_FMT_plx "\n", __FUNCTION__,addr,
                  cpu_single_env->active_tc.PC);

    }
    return 0;
}

/*TODO: do the real cpu->pci mapping*/
static void loongson_addwin_write32(void *opaque, target_phys_addr_t addr,
                                  uint32_t value)
{
    struct loongson_addrwin_s *s = (struct loongson_addrwin_s *) opaque;

    debug_out(DEBUG_ADDR_WIN,
              ("%s addr " TARGET_FMT_plx " value %x pc " TARGET_FMT_plx "\n",
              __FUNCTION__, addr, value, cpu_single_env->active_tc.PC));
    switch (addr)
    {
    	case 0x00:
    	case 0x08:
    	case 0x10:
    	case 0x18:
    		s->cpu_win_base_low[addr/0x8] = value;
    		break;
    	case 0x04:
    	case 0x0c:
    	case 0x14:
    	case 0x1c:
    		s->cpu_win_base_high[(addr-0x4)/0x8] = value;
    		break;
    	case 0x20:
    	case 0x28:
    	case 0x30:
    	case 0x38:
    		s->cpu_win_mask_low[(addr-0x20)/0x8] = value;
    		break;
    	case 0x24:
    	case 0x2c:
    	case 0x34:
    	case 0x3c:
    		s->cpu_win_mask_high[(addr-0x24)/0x8] = value;
    		break;
    	case 0x40:
    	case 0x48:
    	case 0x50:
    	case 0x58:
    		s->cpu_win_mmap_low[(addr-0x40)/0x8] = value;
    		break;
    	case 0x44:
    	case 0x4c:
    	case 0x54:
    	case 0x5c:
    		s->cpu_win_mmap_high[(addr-0x44)/0x8] = value;
    		break;

    	case 0x60:
    	case 0x68:
    	case 0x70:
    	case 0x78:
    		s->pcidma_win_base_low[(addr-0x60)/0x8] = value;
    		break;
    	case 0x64:
    	case 0x6c:
    	case 0x74:
    	case 0x7c:
    		s->pcidma_win_base_high[(addr-0x64)/0x8] = value;
    		break;
    	case 0x80:
    	case 0x88:
    	case 0x90:
    	case 0x98:
    		s->pcidma_win_mask_low[(addr-0x80)/0x8] = value;
    		break;
    	case 0x84:
    	case 0x8c:
    	case 0x94:
    	case 0x9c:
    		s->pcidma_win_mask_high[(addr-0x84)/0x8] = value;
    		break;
    	case 0xa0:
    	case 0xa8:
    	case 0xb0:
    	case 0xb8:
    		s->pcidma_win_mmap_low[(addr-0xa0)/0x8] = value;
    		break;
    	case 0xa4:
    	case 0xac:
    	case 0xb4:
    	case 0xbc:
    		s->pcidma_win_mmap_high[(addr-0xa4)/0x8] = value;
    		break;
    	default:
        	cpu_abort(cpu_single_env,
                  "%s undefined addr " TARGET_FMT_plx
                  "pc " TARGET_FMT_plx "\n", __FUNCTION__,addr,
                  cpu_single_env->active_tc.PC);
    	}

}


CPUReadMemoryFunc *loongson_addwin_readfn[] = {
    loongson_badwidth_read32,
    loongson_badwidth_read32,
    loongson_addwin_read32,
};

CPUWriteMemoryFunc *loongson_addwin_writefn[] = {
    loongson_badwidth_write32,
    loongson_badwidth_write32,
    loongson_addwin_write32,
};

static void loongson_addwin_reset(struct loongson_addrwin_s *s)
{
	s->cpu_win_base_low[0] = 0x0;
	s->cpu_win_base_high[0] = 0x0;
	s->cpu_win_base_low[1] = 0x10000000;
	s->cpu_win_base_high[1] = 0x0;
	s->cpu_win_base_low[2] = 0xfff00000;
	s->cpu_win_base_high[2] = 0xffffffff;
	s->cpu_win_base_low[3] = 0xfff00000;
	s->cpu_win_base_high[3] = 0xffffffff;


	s->cpu_win_mask_low[0] = 0xf0000000;
	s->cpu_win_mask_high[0] = 0xffffffff;
	s->cpu_win_mask_low[1] = 0xf0000000;
	s->cpu_win_mask_high[1] = 0xffffffff;
	s->cpu_win_mask_low[2] = 0xfff00000;
	s->cpu_win_mask_high[2] = 0xffffffff;
	s->cpu_win_mask_low[3] = 0xfff00000;
	s->cpu_win_mask_high[3] = 0xffffffff;

	s->cpu_win_mmap_low[0] = 0x0;
	s->cpu_win_mmap_high[0] = 0x0;
	s->cpu_win_mmap_low[1] = 0x10000001;
	s->cpu_win_mmap_high[1] = 0x0;
	s->cpu_win_mmap_low[2] = 0x0;
	s->cpu_win_mmap_high[2] = 0x0;
	s->cpu_win_mmap_low[3] = 0x0;
	s->cpu_win_mmap_high[3] = 0x0;


	s->pcidma_win_base_low[0] = 0x80000000;
	s->pcidma_win_base_high[0] = 0x0;
	s->pcidma_win_base_low[1] = 0xfff00000;
	s->pcidma_win_base_high[1] = 0xffffffff;
	s->pcidma_win_base_low[2] = 0xfff00000;
	s->pcidma_win_base_high[2] = 0xffffffff;
	s->pcidma_win_base_low[3] = 0xfff00000;
	s->pcidma_win_base_high[3] = 0xffffffff;


	s->pcidma_win_mask_low[0] = 0xfff00000;
	s->pcidma_win_mask_high[0] = 0xffffffff;
	s->pcidma_win_mask_low[1] = 0xfff00000;
	s->pcidma_win_mask_high[1] = 0xffffffff;
	s->pcidma_win_mask_low[2] = 0xfff00000;
	s->pcidma_win_mask_high[2] = 0xffffffff;
	s->pcidma_win_mask_low[3] = 0xfff00000;
	s->pcidma_win_mask_high[3] = 0xffffffff;

	s->pcidma_win_mmap_low[0] = 0x0;
	s->pcidma_win_mmap_high[0] = 0x0;
	s->pcidma_win_mmap_low[1] = 0xfff00000;
	s->pcidma_win_mmap_high[1] = 0xffffffff;
	s->pcidma_win_mmap_low[2] = 0xfff00000;
	s->pcidma_win_mmap_high[2] = 0xffffffff;
	s->pcidma_win_mmap_low[3] = 0xfff00000;
	s->pcidma_win_mmap_high[3] = 0xffffffff;

	
}

void loongson_addwin_init()
{
    int iomemtype;
    struct loongson_addrwin_s *s;

    s = qemu_mallocz(sizeof(*s));
    iomemtype =
        cpu_register_io_memory(0, loongson_addwin_readfn, loongson_addwin_writefn,
                               s);
    cpu_register_physical_memory(0x3ff00000, 0x1000, iomemtype);
    loongson_addwin_reset(s);
}


