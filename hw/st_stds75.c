/*
 * QEMU st STDS75 Digital temperature sensor and thermal watchdog emulation
 *
 * Copyright (c) 2009 yajin <yajin@vm-kernel.org>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */




#include "hw.h"
#include "qemu-timer.h"
#include "i2c.h"
#include "sysemu.h"
#include "console.h"


struct stds75_s
{
    i2c_slave i2c;
    qemu_irq irq;

    int firstbyte;
    uint8_t reg;
    uint8_t index;

    uint16_t temp;
    uint8_t conf;
    uint16_t thys;
    uint16_t tos;
};

static void stds75_event(i2c_slave * i2c, enum i2c_event event)
{
    struct stds75_s *s = (struct stds75_s *) i2c;
    //printf("stds75_event %d \n", event);
    if (event == I2C_START_SEND)
        s->firstbyte = 1;
}

static int stds75_rx(i2c_slave * i2c)
{
    struct stds75_s *s = (struct stds75_s *) i2c;
    //printf("stds75_rx s->reg %d \n", s->reg);
    uint8_t ret = 0;

    switch (s->reg)
    {
    case 0x0:
        ret = (s->temp >> ((1 - s->index) * 8)) & 0xff;
        break;
    case 0x1:
        ret = s->conf;
        break;
    case 0x2:
        /*first byte is the MSB */
        ret = (s->thys >> ((1 - s->index) * 8)) & 0xff;
        break;
    case 0x03:
        ret = (s->tos >> ((1 - s->index) * 8)) & 0xff;
        break;
    default:
        fprintf(stderr, "%s: read from index %d of stds75 \n",
                __FUNCTION__, s->reg);
        break;
    }
    s->index++;
    return ret;
}

static int stds75_tx(i2c_slave * i2c, uint8_t data)
{
    struct stds75_s *s = (struct stds75_s *) i2c;
    //printf("stds75_tx %d \n", data);
    if (s->firstbyte)
    {
        s->reg = data;
        s->firstbyte = 0;
        s->index = 0;
    }
    else
    {
        switch (s->reg)
        {
        case 0x0:
            fprintf(stderr, "%s: write to index 0 of stds75 \n", __FUNCTION__);
            break;
        case 0x1:
            s->conf = data;
            break;
        case 0x2:
            /*first byte is the MSB */
            s->thys &= (0xff) << (s->index * 8);
            s->thys |= data << ((1 - s->index) * 8);
            s->index++;
            break;
        case 0x03:
            s->tos &= (0xff) << (s->index * 8);
            s->tos |= data << ((1 - s->index) * 8);
            s->index++;
            break;
        default:
            fprintf(stderr, "%s: write to index %d of stds75 \n",
                    __FUNCTION__, s->reg);
            break;

        }

    }

    return 0;
}


static void stds75_reset(struct stds75_s *s)
{
    s->thys = 0x4800;
    s->tos = 0x5000;
}

i2c_slave *stds75_init(i2c_bus * bus, qemu_irq irq, int i2c_address)
{
    struct stds75_s *s = (struct stds75_s *)
        i2c_slave_init(bus, i2c_address, sizeof(struct stds75_s));

    s->i2c.event = stds75_event;
    s->i2c.recv = stds75_rx;
    s->i2c.send = stds75_tx;

    s->irq = irq;

    stds75_reset(s);

    //register_savevm("menelaus", -1, 0, menelaus_save, menelaus_load, s);

    return &s->i2c;
}
