/*
 * QEMU sm502 emulation
 *
 * Copyright (c) 2009 yajin <yajin@vm-kernel.org>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/*sm502 is used in gdium system.*/

#include <stdio.h>
#include <assert.h>
#include "hw.h"
#include "pc.h"
#include "pci.h"
#include "console.h"
#include "devices.h"
#include "sm502.h"

//#define DEBUG
#define DEBUG_PCICONF                     (1<<0x0)
#define DEBUG_CONF                           (1<<0x1)
#define DEBUG_GPIO                           (1<<0x2)
#define DEBUG_MMIO								(1<<0x3)

#define DEBUG_ALL                         (0xffffffff)
#define  DEBUG_FLAG                    DEBUG_MMIO

#ifdef DEBUG
#define debug_out(flag,out) {\
	if (flag & DEBUG_FLAG) printf out; \
	} while(0);
#else
#define debug_out(flag,out)
#endif

//#define PRINT_WARNNING

#if 0
static uint32_t sm502_badwidth_read8(void *opaque, target_phys_addr_t addr)
{
    SM502_8B_REG(addr);
    /*to make debug easy */
    exit(-1);
    return 0;
}

static void sm502_badwidth_write8(void *opaque, target_phys_addr_t addr,
                                  uint32_t value)
{
    SM502_8B_REG(addr);
    exit(-1);
}

static uint32_t sm502_badwidth_read16(void *opaque, target_phys_addr_t addr)
{
    SM502_16B_REG(addr);
    /*to make debug easy */
    exit(-1);
    return 0;
}
static void sm502_badwidth_write16(void *opaque, target_phys_addr_t addr,
                                   uint32_t value)
{
    SM502_16B_REG(addr);
    exit(-1);
}
#endif
static uint32_t sm502_badwidth_read32(void *opaque, target_phys_addr_t addr)
{
    SM502_32B_REG(addr);
    /*to make debug easy */
    exit(-1);
    return 0;
}
static void sm502_badwidth_write32(void *opaque, target_phys_addr_t addr,
                                   uint32_t value)
{
    SM502_32B_REG(addr);
    exit(-1);
}

void sm502_gpio_out_set(struct sm502_s *s, int line, qemu_irq handler)
{
    if (line >= 64 || line < 0)
        cpu_abort(cpu_single_env, "%s: No GPIO line %i\n", __FUNCTION__, line);
    s->out_handler[line & 63] = handler;
}

qemu_irq *sm502_gpio_in_get(struct sm502_s *s, int line)
{
    if (line >= 64 || line < 0)
        cpu_abort(cpu_single_env, "%s: No GPIO line %i\n", __FUNCTION__, line);
    return s->in_handler + (line & 31);
}

void sm502_gpio_set_pin(struct sm502_s *s, int line, int level)
{
    if (line >= 64 || line < 0)
        cpu_abort(cpu_single_env, "%s: No GPIO line %i\n", __FUNCTION__, line);
    if (level)
    {
        if (line < 32)
            s->gpio_data_low |= level << line;
        else
            s->gpio_data_high |= level << (line - 32);
    }
    else
    {
        if (line < 32)
            s->gpio_data_low &= (~(1 << line));
        else
            s->gpio_data_high &= (~(1 << (line - 32)));
    }
}

int sm502_gpio_get_pin(struct sm502_s *s, int line)
{
    if (line >= 64 || line < 0)
        cpu_abort(cpu_single_env, "%s: No GPIO line %i\n", __FUNCTION__, line);
    if (line < 32)
        return (s->gpio_data_low & (1 << line)) >> line;
    else
        return (s->gpio_data_high & (1 << line)) >> line;
}

static void sm502_gpio_set(void *opaque, int line, int level)
{
    struct sm502_s *s = (struct sm502_s *) opaque;
    if (line >= 64 || line < 0)
        cpu_abort(cpu_single_env, "%s: No GPIO line %i\n", __FUNCTION__, line);
    sm502_gpio_set_pin(s, line, level);
}

static void sm502_gpio_init(struct sm502_s *s)
{
    s->in_handler = qemu_allocate_irqs(sm502_gpio_set, s, 64);
}

static uint32_t sm502_read32(void *opaque, target_phys_addr_t addr)
{
    struct sm502_s *s = (struct sm502_s *) opaque;

    debug_out(DEBUG_CONF,
              ("%s addr " TARGET_FMT_plx " pc " TARGET_FMT_plx "\n",
               __FUNCTION__, addr, cpu_single_env->active_tc.PC));

    switch (addr)
    {
    case 0x0:
        return s->system_control;
    case 0x4:
        return s->misc_control;
    case 0x08:
        return s->gpio_low32;
    case 0x0c:
        return s->gpio_high32;
    case 0x10:
        return s->dram_control;
    case 0x14:
        return s->arb_control;
    case 0x24:
        return s->command_list_status;
    case 0x28:
        return s->raw_int_status;
    case 0x2c:
        return s->int_status;
    case 0x30:
        return s->int_mask;
    case 0x34:
        return s->debug_control;
    case 0x38:
        return s->current_gate;
    case 0x3c:
        return s->current_clock;
    case 0x40:
        return s->power0_gate;
    case 0x44:
        return s->power0_clock;
    case 0x48:
        return s->power1_gate;
    case 0x4c:
        return s->power1_clock;
    case 0x50:
        return s->sleep_gate;
    case 0x54:
        return s->power_control;
    case 0x58:
        return s->pci_master_base;
    case 0x5c:
        return s->endian_control;
    case 0x60:
        return s->device_id;
    case 0x64:
        return s->pll_clock_count;
    case 0x68:
        return s->misc_timing;
    case 0x6c:
        return s->current_sdram_clock;
    case 0x70:
        return s->non_cache_address;
    case 0x74:
        return s->pll_control;
    case 0x10000:
        return s->gpio_data_low;
    case 0x10004:
        return s->gpio_data_high;
    case 0x10008:
        return s->gpio_datadir_low;
    case 0x1000c:
        return s->gpio_datadir_high;
    case 0x10010:
        return s->gpio_int_setup;
    case 0x10014:
        return s->gpio_int_status;
    default:
        cpu_abort(cpu_single_env,
                  "%s undefined addr " TARGET_FMT_plx
                  "  pc " TARGET_FMT_plx "\n", __FUNCTION__, addr,
                  cpu_single_env->active_tc.PC);

    }

}

static void sm502_write32(void *opaque, target_phys_addr_t addr, uint32_t value)
{
    struct sm502_s *s = (struct sm502_s *) opaque;
    uint32_t diff;
    int ln;

    debug_out(DEBUG_CONF,
              ("%s addr " TARGET_FMT_plx " value %x pc " TARGET_FMT_plx "\n",
               __FUNCTION__, addr, value, cpu_single_env->active_tc.PC));


    switch (addr)
    {
    case 0x24:
    case 0x38:
    case 0x3c:
    case 0x60:
    case 0x64:
    case 0x6c:
        SM502_RO_REG(addr);
        break;
    case 0x0:
        s->system_control = value & 0xff00b8f7;
        break;
    case 0x4:
        s->misc_control = value & 0xff7fff10;
        break;
    case 0x08:
        s->gpio_low32 = value;
        break;
    case 0x0c:
        s->gpio_high32 = value;
        break;
    case 0x10:
        s->dram_control = value & 0x7fffffc3;
        break;
    case 0x14:
        s->arb_control = value & 0x77777777;
        break;
    case 0x28:
        s->raw_int_status &= (~(value & 0x7f));
        break;
    case 0x2c:
        s->int_status = value & 0x400000;
        break;
    case 0x30:
        s->int_mask = value & 0xffdf3f5f;
        break;
    case 0x34:
        s->debug_control = value & 0xff;
        break;
    case 0x40:
        s->power0_gate = value & 0x71dff;
        break;
    case 0x44:
        s->power0_clock = value & 0xff3f1f1f;
        break;
    case 0x48:
        s->power1_gate = value & 0x61dff;
        break;
    case 0x4c:
        s->power1_clock = value & 0xff3f1f1f;
        break;
    case 0x50:
        s->sleep_gate = value & 0x786000;
        break;
    case 0x54:
        s->power_control = value & 0x7;
        break;
    case 0x58:
        s->pci_master_base = value & 0xfff00000;
        break;
    case 0x5c:
        s->endian_control = value & 0x1;
        break;
    case 0x68:
        s->misc_timing = value & 0xf1f1f4f;
        break;
    case 0x70:
        s->non_cache_address = value & 0x3fff;
        break;
    case 0x74:
        s->pll_control = value & 0x1fffff;
        break;
    case 0x10000:
        diff = (s->gpio_data_low ^ value) & s->gpio_datadir_low;
        s->gpio_data_low = value;
        while ((ln = ffs(diff)))
        {
            ln--;
            if (s->out_handler[ln])
                qemu_set_irq(s->out_handler[ln], (value >> ln) & 1);
            diff &= ~(1 << ln);
        }
        break;
    case 0x10004:
        diff = (s->gpio_datadir_high ^ value) & s->gpio_datadir_high;
        s->gpio_data_high = value;
        while ((ln = ffs(diff)))
        {
            ln--;
            if (s->out_handler[ln + 32])
                qemu_set_irq(s->out_handler[ln + 32], (value >> ln) & 1);
            diff &= ~(1 << ln);
        }
        break;
    case 0x10008:
        s->gpio_datadir_low = value;
        break;
    case 0x1000c:
        s->gpio_datadir_high = value;
        break;
    case 0x10010:
        s->gpio_int_setup = value & 0x7f7f7f;
        break;
    case 0x10014:
        s->gpio_int_status &= (~(value & 0x7f0000));
        break;
    default:
        cpu_abort(cpu_single_env,
                  "%s undefined addr " TARGET_FMT_plx
                  "  value %x pc " TARGET_FMT_plx "\n", __FUNCTION__, addr,
                  value, cpu_single_env->active_tc.PC);


    }

}


static CPUReadMemoryFunc *sm502_readfn[] = {
    sm502_badwidth_read32,
    sm502_badwidth_read32,
    sm502_read32,
};

static CPUWriteMemoryFunc *sm502_writefn[] = {
    sm502_badwidth_write32,
    sm502_badwidth_write32,
    sm502_write32,
};

static uint32_t sm502_read_config(PCIDevice * d, uint32_t address, int len)
{
    uint32_t val = 0;

    debug_out(DEBUG_PCICONF, ("%s addr 0x%x  len %x pc " TARGET_FMT_plx "\n",
                              __FUNCTION__, address, len,
                              cpu_single_env->active_tc.PC));

    switch (len)
    {
    default:
    case 4:
        if (address <= 0xfc)
            val = le32_to_cpu(*(uint32_t *) (d->config + address));
        break;

    case 2:
        if (address <= 0xfe)
            val = le16_to_cpu(*(uint16_t *) (d->config + address));
        break;
    case 1:
        val = d->config[address];
        break;
    }
    return val;

}

static void sm502_write_config(PCIDevice * d, uint32_t address, uint32_t val,
                               int len)
{
    int can_write, i;
    uint32_t addr;
    uint32_t new_sm502_mm_io;
    uint8_t write_mask;

    struct sm502_s *s = (struct sm502_s *) d;

    debug_out(DEBUG_PCICONF,
              ("%s addr 0x%x value %x len %x pc " TARGET_FMT_plx "\n",
               __FUNCTION__, address, val, len, cpu_single_env->active_tc.PC));

    /* not efficient, but simple */
    addr = address;
    can_write = 1;
    write_mask = 0xff;
    for (i = 0; i < len; i++)
    {
        /* default read/write accesses */
        switch (addr)
        {
        case 0x00:
        case 0x01:
        case 0x02:
        case 0x03:
        case 0x08:
        case 0x09:
        case 0x0a:
        case 0x0b:
        case 0x0c:
        case 0x0d:
        case 0x2c:
        case 0x2d:
        case 0x2e:
        case 0x2f:
        case 0x34:
        case 0x35:
        case 0x36:
        case 0x37:
        case 0x40:
        case 0x41:
        case 0x42:
        case 0x43:
            can_write = 0;
            break;

        case 0x05:
        case 0x06:
        case 0x07:
        case 0x10:
        case 0x11:
        case 0x12:             /*[23:21] of reg10 -> 0x0? */
        case 0x14:
        case 0x15:
        case 0x31:
        case 0x3d:
        case 0x3e:
        case 0x3f:
        case 0x46:
        case 0x47:
            write_mask = 0;
            break;
        case 0x04:
            write_mask = 0x36;
            break;
        case 0x13:
            write_mask = 0xfe;
            break;
        case 0x16:
            write_mask = 0xe0;
            break;
        case 0x17:
            write_mask = 0xff;
            break;
        case 0x30:
            if (d->config[4] & 0x2)
                write_mask = 0x1;
            else
                write_mask = 0x0;
            break;
        case 0x32:
        case 0x33:
            if (d->config[4] & 0x2)
                write_mask = 0xff;
            else
                write_mask = 0x0;
            break;
        case 0x3c:
        case 0x44:
        case 0x45:
            write_mask = 0xff;
            break;
        default:
            /*reserved */
            can_write = 2;
            break;
        }
        if (can_write == 1)
        {
            d->config[addr] = (val & write_mask);
            if (addr == (0x14 + len - 1))
            {
                /*write the last byte of mmio */
                new_sm502_mm_io = le32_to_cpu(*(uint32_t *) (d->config + 0x14));
                if ((s->sm502_mm_io != new_sm502_mm_io)
                    && ((pci_mem_base + new_sm502_mm_io) < 0xffffffff))
                {
                    if (s->mmio_index != 0)
                    {
                        cpu_register_physical_memory(pci_mem_base +
                                                     s->sm502_mm_io, 0x200000,
                                                     IO_MEM_UNASSIGNED);
                        cpu_unregister_io_memory(s->mmio_index);
                    }
                    /*remapped the Control register to CPU memory space */
                    s->mmio_index =
                        cpu_register_io_memory(0, sm502_readfn,
                                               sm502_writefn, s);
                    cpu_register_physical_memory(pci_mem_base +
                                                 new_sm502_mm_io, 0x200000,
                                                 s->mmio_index);
                    debug_out(DEBUG_MMIO,
                              ("new_sm502_mm_io %x pci_mem _base %llx\n",
                               new_sm502_mm_io, pci_mem_base));
                }
            }
        }
        else if (can_write == 0)
        {
#ifdef PRINT_WARNNING
            fprintf(stderr,
                    "warnning. %s :write to read only pci conf register addr %x\n",
                    __FUNCTION__, addr);
#endif
        }
        else if (can_write == 2)
        {
#ifdef PRINT_WARNNING
            fprintf(stderr,
                    "warnning. %s :write to reserved pci conf register addr %x\n",
                    __FUNCTION__, addr);
#endif
        }
        if (++addr > 0xff)
            break;
        val >>= 8;
    }
}

static void sm502_reset(struct sm502_s *s)
{
    uint8_t *pci_conf;

    pci_conf = s->dev.config;
    memset(pci_conf, 0, 256);

    pci_conf[0] = 0x6F;
    pci_conf[1] = 0x12;
    pci_conf[2] = 0x01;
    pci_conf[3] = 0x05;
    pci_conf[6] = 0x30;
    pci_conf[7] = 0x02;
    pci_conf[8] = 0xc0;
    pci_conf[10] = 0x80;
    pci_conf[11] = 0x03;
    pci_conf[0x34] = 0x40;
    pci_conf[0x40] = 0x01;
    pci_conf[0x42] = 0x01;
    pci_conf[0x43] = 0x06;

    s->arb_control = 0x05146732;
    s->current_gate = 0x00021807;
    s->current_clock = 0x2a1a0a09;
    s->power0_gate = 0x00021807;
    s->power0_clock = 0x2a1a0a09;
    s->power1_gate = 0x00021807;
    s->power1_clock = 0x2a1a0a09;
    s->sleep_gate = 0x00018000;
    s->endian_control = 0x1;
    s->device_id = 0x050100a0;
    s->non_cache_address = 0x0000ffff;
    s->pll_control = 0x0000ffff;

}

struct sm502_s *sm502_init(PCIBus * bus, int devfn, target_phys_addr_t pci_base)
{
    struct sm502_s *s = (struct sm502_s *) qemu_mallocz(sizeof(*s));

    s = (struct sm502_s *) pci_register_device(bus, "SM502 PCI",
                                               sizeof(struct sm502_s), devfn,
                                               sm502_read_config,
                                               sm502_write_config);
    pci_mem_base = pci_base;

    sm502_gpio_init(s);

    sm502_reset(s);

    return s;
}
