#!/bin/bash
# Copyright (C) 2009 yajin (yajin@vm-kernel.org)
#
# pad the BIOS image of gdium to 512K bytes
#
#
# gdium_bios_pad.sh  <original_bios_image>   <output_bios_image>  <output_bios_size>
#
#

if [ ! -r "$1" ]; then
        echo "Usage: $0 <original_bios_image>   <output_bios_image>  <output_bios_size>"
        exit -1
fi

original_bios_image=$1
bios_image_name=$2
bios_image_len=$3
bios_kbs=$[$bios_image_len/1024]


if [ -e "$2" ]; then
  echo $bios_image_name" exist.Delete it first!"
  rm -rf $bios_image_name
fi

echo -en \\0377\\0377\\0377\\0377\\0377\\0377\\0377\\0377 > .8b
cat .8b .8b >.16b
cat .16b .16b >.32b
cat .32b .32b .32b .32b >.128b
cat .128b .128b .128b .128b >.512b
cat .512b .512b >.1024b
 
i=0
while  [ $i -lt $bios_kbs  ]
do
  cat .1024b>>$bios_image_name
  i=$[$i + 1]
done
  
dd if=$original_bios_image of=$bios_image_name  conv=notrunc seek=0
rm .8b .16b .32b .128b .512b .1024b
